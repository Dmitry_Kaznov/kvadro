var gulp = require('gulp'),
    sass = require('gulp-sass'),
    autoprefixer = require('gulp-autoprefixer'),
    cleancss = require('gulp-clean-css'),
    imagemin = require('gulp-imagemin'),
    svgSprite = require('gulp-svg-sprites'),
    svgmin = require('gulp-svgmin'),
    cheerio = require('gulp-cheerio'),
    replace = require('gulp-replace'),
    spritesmith = require('gulp.spritesmith');

gulp.task('sprite', function () {
    var spriteData = gulp.src('./src/images/*.*')
        .pipe(spritesmith({
            imgName: 'sprite.png',
            cssName: '_sprite.css'
        }));

    spriteData.img.pipe(gulp.dest('./build/images/'));
    spriteData.css.pipe(gulp.dest('./src/sass/'));
});



gulp.task('sass-to-css', function(){
   gulp.src('./src/sass/main.scss')
       .pipe(sass())
       .pipe(autoprefixer({
           browsers: ['last 2 versions'],
           cascade: false
       }))
       .pipe(cleancss({compatibility: 'ie8'}))
       .pipe(gulp.dest('./build/css'))
});



gulp.task('svg-sprite', function () {
    return gulp.src('./src/svg/*.svg')
    // minify svg
        .pipe(svgmin({
            js2svg: {
                pretty: true
            }
        }))
        // remove all fill and style declarations in out shapes
        .pipe(cheerio({
            run: function ($) {
                $('[fill]').removeAttr('fill');
                $('[style]').removeAttr('style');
            },
            parserOptions: { xmlMode: true }
        }))
        // cheerio plugin create unnecessary string '>', so replace it.
        .pipe(replace('&gt;', '>'))
        // build svg sprite
        .pipe(svgSprite({
                preview: false,
                selector: "icon-%f",
                svg: {
                    symbols: 'symbol_sprite.html'
                },
                cssFile: './src/sass/'

            }
        ))
        .pipe(gulp.dest('./build/svg'));
});

gulp.task('image-min', function () {
    gulp.src('./src/images/*.*')
        .pipe(imagemin())
        .pipe(gulp.dest('./build/images'))
});


